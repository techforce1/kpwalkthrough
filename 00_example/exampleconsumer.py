# simpleconsumer.py:
#
#
# consume stream van fixed topic en print stream op scherm
#
#

import faust

app = faust.App('exampleconsumer', broker='kafka-1:19092')
topic = app.topic('example')

@app.agent(topic)
async def process(stream):
    async for value in stream:
        print(value)

if __name__=="__main__":
    app.main()