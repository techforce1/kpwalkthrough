"""
Simpele Faust based producer die:
- topic aanmaakt nl. example
- bericht op dat topic produced 
"""
import faust

# maak Faust app aan
app = faust.App('exampleproducer', broker='kafka-1:19092')
topic = app.topic('example')

# registreer een async func (send_message):
# produceer een string (value) elke (interval) seconden
@app.timer(interval=1.0)
async def send_message(message):
    await topic.send(value="Python & Kafka met Techforce1")

if __name__ == '__main__':

    # Start de Faust async eventloop
    app.main()
